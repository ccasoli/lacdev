<?php

class Users extends CI_Controller
{
	//donnée de la session
	private $session_id;

	public $name;
	private $password;

	//constructeur appele a la connexion au cas ou la session aurait changé
	public function __construct()
	{
		parent::__construct();
	}


	public function index()
	{
		$this->name     = $this->input->post('name');
		$this->password = $this->input->post('password');
		$this->connexion();
	}

	//methode de depart permet de faire une velidation du formulaire 
	//avec des regles de la blibil form_validation
	//regles :
	//
	public function connexion()
	{
		//chargement de la bibliothèque formulaire
		$this->load->library('form_validation');
		//on definie les regles
		$this->form_validation->set_rules('name', '"Nom d\'utilisateur"', 'trim|required|min_length[3]|max_length[52]');
		$this->form_validation->set_rules('password',    '"Mot de passe"', 'trim|required|min_length[3]|max_length[52]');

		//si les regles sont respetées on continu
		if($this->form_validation->run())
		{
			//$this->session($this->name);
			$this->admin();
		}
		else
		{
			echo $this->name;
			$this->load->view('user/user');
		}
	}


	public function session()
	{
		//on donne une valeur a la session 
		$this->session->set_userdata('session_id', $name);
		//on la recupere
		$session_id = $this->session->userdata('session_id');
		//on verifie si c est une nouvelle session

		//echo 'id: ' .$session_id;
	}

	//si le formulaire de connexion est valider alors on passe par la
	//cette methode verifie si l'utilisateur est un admin ou non et redirige en fonction
	public function admin()
	{
		//recupere les informations de l'utilisateurs
		$user = $this->user_model->get_info($this->name);

		//on verifie si l user exite deja
		if($user)
		{
			foreach($user as $info)
			{
				//verifie que le mdp soit correct
				if($this->password == $info->mdp)
				{
					//si c est un admin
					if($info->admin == 1)
					{
						//on charge la page admin
						$this->load->view( 'user/admin' , $user);
					}
					else
					{
						//ou la page client
						$this->load->view('user/client', $user);
					}
				}
				else
				{
					//mauvais mot de passe
					$this->load->view('user/user' ,$this->name);
					echo 'mauvais mot de passe.';
				}
			}
		}
		else
		{	
			//redirection vers le forùulaire d'inscription
			//$this->inscription();
		}
	}


	public function deconnexion()
	{
		//	Détruit la session
		$this->session->sess_destroy();

		//	Redirige vers la page d'accueil
		redirect();
	}


	public function inscription($name)
	{
		
		//chargement de la bibliothèque formulaire
		$this->load->library('form_validation');
		//on definie les regles
		$this->form_validation->set_rules('name', '"Nom d\'utilisateur"', 'trim|required|min_length[3]|max_length[52]');
		$this->form_validation->set_rules('password',    '"Mot de passe"', 'trim|required|min_length[3]|max_length[52]');

		//si les regles sont respetées on continu
		if($this->form_validation->run())
		{
			//recupere les donnée du formulaire
			$name     = $this->input->post('name');
			$password = $this->input->post('password');
			$email    = $this->input->post('email');

			//si tout les ccritere sont remplis on envoi un email au superadmin

		}
		else
		{
			$this->load->view('user/inscription', $name);
		}
	}


	/*//	L'affichage de la variable $output est le comportement par défaut.
	public function _output($output)
	{
		var_dump($output);
	}*/
}